
PFLOTRAN WITH CPR PRECONDITIONER
--------

This is a WIP project to add CPR-AMG functionallity to PFLOTRAN. This is a two stage preconditioner
which first applies algebraic multigrid to an extracted pressure system, then applies a simple
preconditioner to the whole system, which is corrected by the solution to the pressure system.


To run in CPR mode, just choose CPR as the preconditioner in the LINEAR_SOLVER card:

    LINEAR_SOLVER FLOW
      PC_TYPE CPR
    END
  
## AMG Configuration
BOOMERAMG from the HYPRE package is the AMG implementation we use. We automatically set some configuration options for BOOMERAMG that are appropriate for the CPR solver. These are equivalent to the command line options:

     -pc_hypre_boomeramg_strong_threshold 0.5 -pc_hypre_boomeramg_coarsen_type PMIS -pc_hypre_boomeramg_interp_type ext+i -pc_hypre_boomeramg_relax_type_all Jacobi
However, note that in doing this we override command line options that you may have added, so if you wish to adjust the configuration of BOOMERAMG yourself then you MUST add the line CPR_MANUAL_AMG_CONFIG to the LINEAER_SOLVER card:

    LINEAR_SOLVER FLOW
      PC_TYPE CPR 
      CPR_MANUAL_AMG_CONFIG 
    END
Then nothing is done with regard to setting default BOOMERAMG options and command line options can be used to configure it as desired.

To help double check how BOOMERAMG is configured, add CPRAMGREPORT to the LINEAR_SOLVER card to enable output a lot of diagnostic info on the setup, after each time BOOMERAMG is called. Not recommended for full runs of course: 

    LINEAR_SOLVER FLOW
      PC_TYPE CPR 
      CPRAMGREPORT
    END

## Choice of KSP solver
PFLOTRAN's default KSP (linear solver) is BCGS. The CPR preconditioner is usually used in (F)GMRES instead. To use FGMRES you can simply specify this in the LINEAR_SOLVER card:

    LINEAR_SOLVER FLOW
      PC_TYPE CPR
      KSP_TYPE FGMRES
    END

HOWEVER, note that the default configuation for (F)GMRES is quite weak. I strongly reccomend command line options like:

    -flow_ksp_gmres_restart 100 -flow_ksp_gmres_modifiedgramschmidt -ksp_gmres_preallocate
